const http = require('http');
const fs = require('fs');

// myReadStream.on('data', function(chunk){
//     console.log('new chunk');
//     myWriteStream.write(chunk);
// });

const server = http.createServer(function(req, res) {
    console.log('req', req.url);
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    let myReadStream = fs.createReadStream(__dirname + '/readMe.txt');
    myReadStream.pipe(res);
});

server.listen(4000, () => console.log('Server running ... '));