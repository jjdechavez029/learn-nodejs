const fs = require('fs');
const path = require('path');

// fs.readFile(path.join(__dirname, '/readMe.txt'), 'utf8', async function(err, data) {
//     if (err) throw err;
//     fs.writeFile(path.join(__dirname, '/writeMe.txt'), data,  function(err) {
//         if (err) throw err;
//         console.log('writeMe.txt was written!')
//     });
// });

// remove file
// fs.unlink('writeMe.txt', (err) => err && err);

// Directory
// fs.mkdir(path.join(__dirname, 'stuff'), err => err && err);
// fs.rmdir(path.join(__dirname, 'stuff'), err => err && err);

// test: make a directory then inside of the directory write a file based on readMe.txt
fs.readFile(path.join(__dirname, 'readMe.txt'), { encoding: 'utf8' }, function(err, data) {
    if (err) throw err;
    fs.mkdir(path.join(__dirname, 'stuff') ,function(err) {
        if (err) throw err;
        fs.writeFile(path.join(__dirname, 'stuff','writeMe.txt'), data, function(err) {
            if (err) throw err;
            console.log('Successful');
        })
    });
});
// fs.mkdir(path.join(__dirname, 'stuff'), function(err) {

// });

// check for the existence of a file before calling fs.open(), fs.readFile() or fs.writeFile()
// fs.stat(path.join(__dirname, 'stuff'), function(err, stat) {
//     if (err) throw err;
//     console.log(stat.isDirectory()) // bool
// });
