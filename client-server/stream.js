const http = require('http');
const fs = require('fs');

const server = http.createServer(function(req, res) {
    console.log('req', req.url);
    res.writeHead(200, { 'Content-Type': 'text/html' });
    let myReadStream = fs.createReadStream(__dirname + '/index.html');
    myReadStream.pipe(res);
});

server.listen(4000, () => console.log('Server running ... '));