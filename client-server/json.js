const http = require('http');
const fs = require('fs');

const server = http.createServer(function(req, res) {
    console.log('req', req.url);
    res.writeHead(200, { 'Content-Type': 'application/json' });
    const myObj = {
        name: 'Jerald',
        age: 20,
        job: 'Developer'
    };
    // res.end receive only string or buffer;
    res.end(JSON.stringify(myObj))
});

server.listen(4000, () => console.log('Server running ... '));