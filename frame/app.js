const express = require('express');
const app = express();

const birds = require('./routes/birds');

app.set('view engine', 'ejs');
app.use('/assets', express.static('public/assets'));

app.get('/', (req, res) => {
    res.render('index');
});

app.use('/birds', birds);

app.get('/contact', (req, res) => {
    res.render('contact');
});

app.get('/profile/:name', (req, res) => {
    console.log(req.params);
    let data = {
        age: 20,
        job: 'ninja',
        hobbies: ['eating', 'sleeping', 'fishing']
    }
    res.render('profile', { person: req.params.name, data });
});

app.listen(3000, () => console.log('Server running ... '));